//
//  ViewController.swift
//  Retro-Calculator
//
//  Created by Juan Ramirez on 4/20/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    enum Operation: String {    //This can be used as type
        case Divide = "/"
        case Multiply = "*"
        case Subtract = "-"
        case Add = "+"
        case Empty = "Empty"    //No operator entered yet
        case Clear = "Clear"
    }
    
    
    //Outlets
    @IBOutlet weak var outputLabel: UILabel!
    
    //Variables
    var runningNumber = ""
    var leftValString = ""
    var rightValString = ""
    var currentOperation: Operation = Operation.Empty
    var result = ""
    var buttonSound: AVAudioPlayer!
    var clearPressedTwice = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("btn", ofType: "wav")
        let soundURL = NSURL(fileURLWithPath: path!)
        
        do{
            //Using this because Xcode throws something
            try buttonSound = AVAudioPlayer(contentsOfURL: soundURL)
            
            buttonSound.prepareToPlay()     //So there is less delay as user presses button
            
        } catch let error as NSError{
            print(error.debugDescription)
        }
        
    }

    //Actions
    @IBAction func numberPressed(button: UIButton){
        
        playButtonSound()
        
        runningNumber += "\(button.tag)"
        outputLabel.text = runningNumber
    }

    @IBAction func onDividePress(sender: AnyObject) {
        
        processOperation(Operation.Divide)
        
    }
    
    @IBAction func onMultiplyPress(sender: AnyObject) {
        
        processOperation(Operation.Multiply)
        
    }
    
    @IBAction func onSubtractPress(sender: AnyObject) {
        
        processOperation(Operation.Subtract)
        
    }
    
    @IBAction func onSumPress(sender: AnyObject) {
        
        processOperation(Operation.Add)
        
    }
    
    @IBAction func onEqualsPress(sender: AnyObject) {
        
        processOperation(currentOperation)
        
        //Terminates a computation by setting the operator to Empty
        currentOperation = Operation.Empty
    }
    
    @IBAction func onClearPress(sender: AnyObject) {
        //handle the press
        playButtonSound()
        
        if clearPressedTwice {
            leftValString = ""
            rightValString = ""
            currentOperation = Operation.Empty
        }
        
        runningNumber = ""
        outputLabel.text = "0"
        clearPressedTwice = true
        
    }
    
    //Contains the logic to do computations
    func processOperation(op: Operation) {
        
        playButtonSound()
        
        //Different operator was pressed cancelling the double press
        clearPressedTwice = false
        
            //Enters in case that user pressed an operation
            if currentOperation != Operation.Empty {
                
                // User pressed another operator without entering another number
                if runningNumber != "" {
                    
                    
                    rightValString = runningNumber
                    runningNumber = ""
                    
                    if currentOperation == Operation.Multiply{
                        
                        result = "\(Double(leftValString)! * Double(rightValString)!)"
                    } else if currentOperation == Operation.Divide {
                        
                        result = "\(Double(leftValString)! / Double(rightValString)!)"
                    } else if currentOperation == Operation.Add {
                        
                        result = "\(Double(leftValString)! + Double(rightValString)!)"
                    } else if currentOperation == Operation.Subtract {
                        
                        result = "\(Double(leftValString)! - Double(rightValString)!)"
                    }
                    
                    leftValString = result
                    outputLabel.text = result
                }
                
                
                currentOperation = op
                
            } else {
                
                // This is the first time an operator was pressed
                leftValString = runningNumber
                runningNumber = ""      //Clear number to input next number
                
                currentOperation = op
            }
    }
    
    
    func playButtonSound(){
        
        if buttonSound.playing {
            buttonSound.stop()
        }
        
        buttonSound.play()
        
    }
    
}

